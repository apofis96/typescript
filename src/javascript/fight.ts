import { FighterDetails} from "./constants/interfaces";

export function fight(firstFighter: FighterDetails, secondFighter: FighterDetails) {
  const sevedFirstFighterHealth = firstFighter.health;
  const sevedSecondFighterHealth = secondFighter.health;
  let winner: FighterDetails;
  while(true)
  {
    secondFighter.health -= getDamage(firstFighter, secondFighter);
    if(secondFighter.health <= 0) {
      winner = firstFighter;
      break;
    }
    firstFighter.health -= getDamage(secondFighter, firstFighter);
    if(firstFighter.health <= 0) {
      winner = secondFighter;
      break;
    }
  }
  firstFighter.health = sevedFirstFighterHealth;
  secondFighter.health = sevedSecondFighterHealth;
  return winner;
}

export function getDamage(attacker: FighterDetails, enemy: FighterDetails) {
  const damage = getHitPower(attacker) - getBlockPower(enemy);
  if(damage <= 0) {
    return 0;
  }
  return damage;
}

export function getHitPower(fighter: FighterDetails) {
  const hitPower = fighter.attack * randomNum(1,2);
  return hitPower;
}

export function getBlockPower(fighter: FighterDetails) {
  const blockPower = fighter.defense * randomNum(1,2);
  return blockPower;
}

function randomNum(min: number, max: number) : number {
    return Math.random() * (max - min) + min;
}