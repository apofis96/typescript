import { createFighter } from './fighterView';
import { showFighterDetailsModal } from './modals/fighterDetails';
import { createElement } from './helpers/domHelper';
import {Fighter, FighterDetails} from './constants/interfaces'
import {getFighterDetails} from './services/fightersService'
import { fight } from './fight';
import { showWinnerModal } from './modals/winner';

export function createFighters(fighters : Fighter[]) {
  const selectFighterForBattle = createFightersSelector();
  const fighterElements = fighters.map(fighter => createFighter(fighter, showFighterDetails, selectFighterForBattle));
  const fightersContainer = createElement({ tagName: 'div', className: 'fighters' });

  fightersContainer.append(...fighterElements);

  return fightersContainer;
}

const fightersDetailsCache = new Map<string, FighterDetails>();

async function showFighterDetails(event: Event, fighter : Fighter) {
  const fullInfo = await getFighterInfo(fighter._id);
  showFighterDetailsModal(fullInfo);
}

export async function getFighterInfo(fighterId: string) {
  let fighterInfo = fightersDetailsCache.get(fighterId);
  if(!fighterInfo) {
    fighterInfo = await getFighterDetails(fighterId);
    fightersDetailsCache.set(fighterId, fighterInfo);
  }
  return fighterInfo;
}

function createFightersSelector() {
  const selectedFighters = new Map<string, FighterDetails>();
  return async function selectFighterForBattle(event: Event, fighter: Fighter) {
    const fullInfo = await getFighterInfo(fighter._id);

    if ( (event.target as HTMLInputElement).checked) {
      selectedFighters.set(fighter._id, fullInfo);
    } else { 
      selectedFighters.delete(fighter._id);
    }

    if (selectedFighters.size === 2) {
      const fighters = selectedFighters.values();
      const winner = fight(fighters.next().value, fighters.next().value);
      showWinnerModal(winner);
    }
  }
}