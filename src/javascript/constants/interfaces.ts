export interface Fighter {
  readonly _id: string;
  name: string;
  source: string;
}

export interface FighterDetails extends Fighter{
  health: number;
  attack: number;
  defense: number;
}