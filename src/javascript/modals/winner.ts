import {Fighter } from "../constants/interfaces";
import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showWinnerModal(fighter: Fighter) {
  const title = 'Winner';
  const bodyElement = createWinner(fighter);
  showModal({ title, bodyElement });
}
  
function createWinner(fighter: Fighter) {
  const winner = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
  const attributes = { src: fighter.source };
  const imgElement = createElement({ tagName: 'img', className: 'fighter-image', attributes });

  nameElement.innerText = `Name : ${fighter.name}`;
  winner.append(nameElement, imgElement);

  return winner;
}