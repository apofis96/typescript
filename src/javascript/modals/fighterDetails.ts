import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
import {FighterDetails} from '../constants/interfaces'

export  function showFighterDetailsModal(fighter : FighterDetails) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter: FighterDetails) {
  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
  const attackElement = createElement({ tagName: 'span', className: 'fighter-detail' });
  const defenseElement = createElement({ tagName: 'span', className: 'fighter-detail' });
  const healthElement = createElement({ tagName: 'span', className: 'fighter-detail' });
  const attributes = { src: fighter.source };
  const imgElement = createElement({ tagName: 'img', className: 'fighter-image', attributes });

  nameElement.innerText = `Name : ${fighter.name}`;
  attackElement.innerText = `Attack : ${fighter.attack}`;
  defenseElement.innerText = `Defense : ${fighter.defense}`;
  healthElement.innerText = `Health : ${fighter.health}`;
  fighterDetails.append(nameElement, attackElement, defenseElement, healthElement, imgElement);

  return fighterDetails;
}